import React from 'react';

const SearchBar = ({ handleSearchBar }) => {
    return (
        <input type="search" placeholder="Search by Title"
            onChange={e => handleSearchBar(e.target.value)} />
    );
}

export default SearchBar;