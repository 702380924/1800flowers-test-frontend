import React from 'react';
import '../css/postEditElement.css'

const PostEditElement = (props) => {
    return (
        <div className="postEditElement_overlay">
            <div className="postEditElement">
                <form>
                    <h4>Edit Form</h4>
                    <label>Title</label>
                    <input type="text" onChange={e => props.onChangeTitle(e.target.value)} value={props.post.title}/>
                    <label>Body</label>
                    <textarea onChange={e => props.onChangeBody(e.target.value)} value={props.post.body}></textarea>
                    <button className="button" onClick={props.discardChanges}>Discard</button>
                    <button className="button" onClick={props.savePost}>Save</button>
                </form>
            </div>
        </div>
    );
}

export default PostEditElement;