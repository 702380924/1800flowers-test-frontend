import React from 'react'
import '../css/postList.css'
import PostListElement from '../container/PostElement';

const PostListUI = ( props ) => {
    return (
        <div className="row post-container">
            <ul>{props.posts.map(post => <PostListElement key={post.id} post={post} />)}</ul>
        </div>);

}
export default PostListUI;