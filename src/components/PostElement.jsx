
import React from 'react'

const PostElementUI = ({ post, updatePost }) => {
    return (
        <li>
            <h5>{post.title}</h5><hr />
            <p>{post.body}</p>
            <button onClick={() => updatePost(post)}><i className="small material-icons">edit</i></button>
        </li>
    );
}

export default PostElementUI;