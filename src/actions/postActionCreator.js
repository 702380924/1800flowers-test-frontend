import { FETCH_POST_FAIL, FETCH_POST_SUCCESS, UPDATE_POST, FETCH_POST_REQUEST } from "./postActionType";

export const fetchPostData = () => ({
    type: FETCH_POST_REQUEST
})

export function fetchPostSuccess(posts) {
    return {
        type: FETCH_POST_SUCCESS,
        payload: {
            posts
        }
    };
}

export function fetchPostFail(error) {
    return {
        type: FETCH_POST_FAIL,
        payload: {
            error
        }
    };
}
export function updatePost(post) {
    return {
        type: UPDATE_POST,
        payload: {
            post
        }
    };
}