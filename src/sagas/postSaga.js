
import { call, put, takeLatest, all } from 'redux-saga/effects'
import { FETCH_POST_REQUEST } from "../actions/postActionType";
import { fetchPostSuccess, fetchPostFail } from '../actions/postActionCreator'
import { getPostRequest } from '../services/postApi';

export function* requestPostFromApi() {
    try {
        const res = yield call(getPostRequest);
        yield put(fetchPostSuccess(res));
    } catch (error) {
        yield put(fetchPostFail(error));
    }
}
export function* watchRequestFetchApi() {
    yield takeLatest(FETCH_POST_REQUEST, requestPostFromApi);
}

export function* rootSaga() {
    yield all([
        watchRequestFetchApi()
    ]);
}