import React, { Component } from 'react'
import { connect } from 'react-redux'
import PostListUI from '../components/PostList';
import Spinner from '../components/Spinner';
import SearchBar from '../components/SearchBar';
import { fetchPostData } from '../actions/postActionCreator';

class PostList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '',
            updatePost: false
        }
    }
    componentDidMount() {
        this.props.requestData();
    }
    usingSearchBar = (keyword, posts) => {
        return posts.filter(post => post.title.toLowerCase().includes(keyword.toLowerCase()))
    }
    render() {
        const { posts, loading, error } = this.props;
        const { keyword } = this.state;
        const postsResult = keyword ? this.usingSearchBar(keyword, posts) : posts
        if (error) {
            return <p>Something when wrong :(</p>
        }
        if (loading) {
            return <Spinner />
        }
        return <React.Fragment>
            <SearchBar handleSearchBar={keyword => this.setState({ keyword: keyword })} />
            <PostListUI posts={postsResult} />
        </React.Fragment>
    }
}
const mapStateToProps = state => ({
    posts: state.posts,
    loading: state.loading,
    error: state.error
});
const mapDispatchToProps = dispatch => ({
    requestData: () => dispatch(fetchPostData())
})

export default connect(mapStateToProps, mapDispatchToProps)(PostList);