import React, { Component } from 'react'
import PostEditElement from '../components/PostEditElement';
import PostElementUI from '../components/PostElement'
import { connect } from 'react-redux'
import { updatePost } from '../actions/postActionCreator';

class PostElement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: props.post,
            editStatus: false,
            editPost: props.post
        }
        this.dispatch = props.dispatch;
    }
    setUpdatePost = () => {
        this.setState({ editStatus: true })
    }
    savePost = () => { 
        this.setState({ ...this.state, post: this.state.editPost, editStatus: false });
        this.dispatch(updatePost(this.state.editPost))
    }
    discardChanges = () => {
        this.setState({ ...this.state, editPost: this.state.post, editStatus: false });
    }
    onChangeTitle = title => {
        this.setState({ ...this.state, editPost: { ...this.state.editPost, title: title } })
    }
    onChangeBody = body => {
        this.setState({ ...this.state, editPost: { ...this.state.editPost, body: body } })
    }
    render() {
        return (
            <div className="postListElement">
                {this.state.editStatus ?
                    <PostEditElement post={this.state.editPost}
                        savePost={this.savePost}
                        discardChanges={this.discardChanges}
                        onChangeTitle={this.onChangeTitle}
                        onChangeBody={this.onChangeBody} /> :
                    <PostElementUI post={this.state.post} updatePost={this.setUpdatePost} />}
            </div>
        );
    }
}

export default connect(null)(PostElement);