import { createStore, compose, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import postReducer from '../reducers/postReducer';
import {rootSaga} from '../sagas/postSaga'

const sagaMiddleware = createSagaMiddleware();
let middlewares = applyMiddleware(sagaMiddleware);
const store = createStore(
    postReducer,
    composeWithDevTools(compose(middlewares))
);
sagaMiddleware.run(rootSaga);
export default store;