
import { call, takeLatest } from 'redux-saga/effects'
import { fetchPostSuccess, fetchPostFail } from '../actions/postActionCreator'
import { FETCH_POST_REQUEST } from '../actions/postActionType'
import { watchRequestFetchApi, requestPostFromApi } from '../sagas/postSaga'
import { getPostRequest } from '../services/postApi'
import { expectSaga } from "redux-saga-test-plan";
import { throwError } from "redux-saga-test-plan/providers";

describe('PostSaga Test', () => {
    const watchObject = watchRequestFetchApi();
    const requestPostActions = requestPostFromApi();
    it('Should take watchRequestFetchApi and then requestPostFromApi', () => {
        expect(watchObject.next().value).toEqual(takeLatest(FETCH_POST_REQUEST, requestPostFromApi));
    })
    it('Should take watchRequestFetchApi be done in the next iteration', () => {
        expect(watchObject.next().done).toBeTruthy();
    });
    it("Should call api function", async () => {
        expect(requestPostActions.next().value).toEqual(call(getPostRequest));
    })
    it("Should dispatch success after calling api", () => {
        const posts = [{ userId: 1, id: 1, title: "title 1", body: "body 1" }]
        return expectSaga(requestPostFromApi)
            .provide([[call(getPostRequest), posts]])
            .put(fetchPostSuccess(posts))
            .run();
    })
    it("Should dispatch success after calling api", () => {
        const error = new Error("Error message");
        return expectSaga(requestPostFromApi)
            .provide([[call(getPostRequest), throwError(error)]])
            .put(fetchPostFail(error))
            .run();
    })
});