

import postReducer from '../reducers/postReducer'
import { FETCH_POST_REQUEST, FETCH_POST_SUCCESS, FETCH_POST_FAIL, UPDATE_POST } from '../actions/postActionType'

describe('Post reducer', () => {
    const initialState = {
        loading: false,
        posts: [],
        error: null
    }
    it('Should return the initial state', () => {
        expect(postReducer(undefined, {})).toEqual(initialState);
    });
    it('Should handle FETCH_POST_REQUEST', () => {
        const action = { type: FETCH_POST_REQUEST };
        expect(postReducer(undefined, action)).toEqual({ ...initialState, loading: true });
    });
    it('Should handle FETCH_POST_SUCCESS', () => {
        const action = { type: FETCH_POST_SUCCESS, payload: { posts: [] } };
        expect(postReducer(undefined, action)).toEqual({ ...initialState, loading: false, posts: [] });
    });
    it('Should handle FETCH_POST_FAIL', () => {
        const action = { type: FETCH_POST_FAIL, payload: { posts: [] } };
        expect(postReducer(undefined, action)).toEqual({ loading: false, error: true, posts: [] });
    });
    it('Should handle UPDATE_POST', () => {
        const oldPost = [{ id: 1, title: "sunt aut", body: "quia architecto" }]
        const updatePost = { id: 1, title: "sunt aut 2", body: "quia architecto 2" }
        const action = { type: UPDATE_POST, payload: { post: updatePost } };
        expect(postReducer({ ...initialState, posts: oldPost }, action)).not.toBe({ ...initialState, posts: oldPost });
    });
});