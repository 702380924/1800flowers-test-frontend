import { FETCH_POST_REQUEST, FETCH_POST_SUCCESS, FETCH_POST_FAIL, UPDATE_POST } from '../actions/postActionType'
import { fetchPostData, fetchPostSuccess, fetchPostFail, updatePost } from '../actions/postActionCreator'


describe('actions', () => {
    it('should create action for request posts ', () => {
        const expectedAction = {
            type: FETCH_POST_REQUEST
        }
        expect(fetchPostData()).toEqual(expectedAction)
    })
    it('should create action for request success', () => {
        const posts = [{ userId: 1, id: 1, title: "title 1", body: "body 1" }]
        const expectedAction = {
            type: FETCH_POST_SUCCESS,
            payload: {
                posts
            }
        }
        expect(fetchPostSuccess(posts)).toEqual(expectedAction)
    })
    it('should create action for request fail', () => {
        const error = "Error Message"
        const expectedAction = {
            type: FETCH_POST_FAIL,
            payload: {
                error
            }
        }
        expect(fetchPostFail(error)).toEqual(expectedAction)
    })
    it('should create action for request fail', () => {
        const post = { userId: 1, id: 1, title: "title 1", body: "body 1" }
        const expectedAction = {
            type: UPDATE_POST,
            payload: { post }
        }
        expect(updatePost(post)).toEqual(expectedAction)
    })
})