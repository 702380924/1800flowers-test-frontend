
import Axios from 'axios'

export const getPostRequest = async()  =>  {
    const result = await Axios.get('http://localhost:3010/posts')
    return result.data;
}