import React, { Component } from 'react';
import { Provider } from 'react-redux'
import store from './store';
import PostList from './container/postList';
import Header from './container/Header';
import './app.css'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="container">
          <Header />
          <PostList />
        </div>
      </Provider>
    );
  }
}

export default App;
