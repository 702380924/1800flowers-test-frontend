import { FETCH_POST_REQUEST, FETCH_POST_SUCCESS, FETCH_POST_FAIL, UPDATE_POST } from "../actions/postActionType";
const initialState = {
    loading: false,
    posts: [],
    error: null
}
const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POST_REQUEST:
            return { ...state, loading: true };
        case FETCH_POST_SUCCESS:
            return { ...state, loading: false, posts: action.payload.posts }
        case FETCH_POST_FAIL:
            return { posts: [], loading: false, error: true }
        case UPDATE_POST:
            const updated = state.posts.map(post => {
                if (post.id === action.payload.post.id) {
                    const { title, body } = action.payload.post
                    return { ...post, title: title, body: body }
                }
                return post
            });
            return { ...state, posts: updated };
        default: return state
    }
}
export default postReducer;